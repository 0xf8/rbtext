# rbtext
Pure-C alternative to something like `lolcat` on linux.

## build
`gcc -o rbtext rbtext.c`
OR
`make`

## usage
`./rbtext <text> [text...]`

## install
`install -m755 rbtext /usr/bin`

## runtime configuration
This is done by setting environment variables.
- `RBTEXT_FULLSCREEN` (default: Enabled) - Clear screen every print (useful for text that will wrap)

### example
Runs rbtext without clearing the screen everytime
`RBTEXT_FULLSCREEN=-1 ./rbtext hello world`