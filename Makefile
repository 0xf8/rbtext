
rbtext: rbtext.c
	$(CC) -o $@ $< $(CFLAGS)


.PHONY: install


install: rbtext
	install -m 755 -o root -g root $< /usr/bin
