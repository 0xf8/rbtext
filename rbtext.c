/*

Copyright 2022-2023 0xf8.dev@proton.me

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>.

*/

#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// How many milliseconds the program will wait until the next print ( DEFAULT =
// 100 )
#define CONF_SLEEP_TIMEMS (long)100
// How many seconds (in addition to millisecond above) the program will wait (
// DEFAULT = 0 )
#define CONF_SLEEP_TIMESEC (time_t)0
// How many times a color should be shown before switching to the next ( DEFAULT
// = 2 )
#define CONF_COLOR_LAG 2

// Don't set to 0
// Disabled = -1
// Enabled = 1
// int FLAG_name = default;
int FLAG_FULLSCREEN = 1;

// RICERS:
// To add colors, simply add the color (according to the format below) to COLORS
// FORMAT: "R;G;B",
const char *COLORS[] = {
    "193;71;87",  // RED
    "193;116;71", // ORANGE
    "193;177;71", // YELLOW
    "149;193;71", // GREEN
    "71;193;161", // CYAN
    "71;165;193", // BLUE
    "155;71;193", // PURPLE
    "193;71;153", // PINK
};

unsigned long COLORS_SIZE = sizeof(COLORS) / sizeof(char *);
unsigned long long t = 0;
void genColors(char *s) {
  char *cstr = (char *)malloc(64 + 1);

  for (unsigned long i = 0; i < strlen(s); i++) {
    unsigned long c = ((t + i) / CONF_COLOR_LAG) % COLORS_SIZE;
    snprintf(cstr, 64, "\e[38;2;%sm", COLORS[c]);
    printf("%s%c\e[0m", cstr, s[i]);
  }

  free(cstr);
}

void set_flag(int *flag, const char *env) {
  char *e = getenv(env);
  if (e != NULL) {
    int v = atoi(e);

    if (v == 0)
      return;
    *flag = v;
  }
}

int main(int argc, char **argv) {
  if (argc < 2) {
    // [ERROR] No arguments given. USAGE: ... <text>
    printf("\e[1;31m[ERROR]\e[0m No arguments given.\n\tUSAGE: \e[34m%s "
           "\e[33m<text>\e[0m\n",
           argv[0]);
    return 1;
  }

  set_flag(&FLAG_FULLSCREEN, "RBTEXT_FULLSCREEN");

  uint reqSize = 0;
  for (int i = 1; i < argc; i++) {
    reqSize += strlen(argv[i]);
  }

  char *text = (char *)malloc(reqSize + argc);

  // Adds all the arguments together with spaces seperating them
  for (int i = 1, j = 0; i < argc; i++) {
    for (unsigned int k = 0; k < strlen(argv[i]); k++, j++)
      text[j] = argv[i][k];

    text[j] = ' ';
    j++;
  }

  struct timespec rem,
      req = {CONF_SLEEP_TIMESEC, CONF_SLEEP_TIMEMS * 1000 * 1000};

  while (1) {
    if (FLAG_FULLSCREEN == 1)
      printf("\e[2J\e[f");
    else
      printf("\r");
    genColors(text);
    fflush(stdout);
    nanosleep(&req, &rem);
    t += 1;
  }

  return 0;
}
